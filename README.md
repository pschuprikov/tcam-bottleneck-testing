# Code for simulations of the scalable TCAM algorithms

## Dependencies

 * [Python][python]: 3.6.4
 * [classifier-lib][clslib]: rev `8389d307`
 * [Click][click]: 3.7
 * [SQLAlchemy][sqlalchemy]: 1.1.15

## Preparation

 * Build `classifier-lib` and add to the `PYTHONPATH`
 * Clone this repo and `cd` into it.

## Running

Run `python runner.py --help` to learn the list of available commands. The most
important one is the `run` command. Additional information for this command can
be learnt by running `python runner.py run --help` that contains the description
of all necessary parameters. 

The `--major` option specifies the independent variable of a plot. The 

### Fig. 5

```bash
python runner.py run --num-actions 1000 --min-class-size 5 --max-class-size 10 \
    --shareable-fraction 0:1:20 --num-flows 1000 --save  --num-runs 50 
    --zipf-s 2 --zipf-s 0 \
    --major shareable_fraction ./classbench/{fw1,ipc2,acl5}.txt
```

### Fig. 6

```bash
python runner.py run --num-actions 1000 --min-class-size 5 \
    --max-class-size 5:500:8:exp \
    --shareable-fraction 0.4 --shareable-fraction 0.6 --shareable-fraction 0.8 \
    --num-flows 1000 --num-runs 50 --save  \
    --major max_class_size ./classbench/{fw1,ipc2,acl5}.txt
```

### Fig. 7

```bash
python runner.py run --num-actions 1000 --min-class-size 5 --max-class-size 10 \
    --shareable-fraction 0.4 --shareable-fraction 0.6 --shareable-fraction 0.8 \
    --num-flows 1000 --num-runs 50 --zipf-s 0:5:10 --save  \
    --major zipf_s ./classbench/{fw1,ipc2,acl5}.txt
```

### Fig. 8

```bash
python runner.py run --num-actions 1000 --min-class-size 5 --max-class-size 10 \
    --shareable-fraction 0.4 --shareable-fraction 0.6 --shareable-fraction 0.8 \
    --num-flows 100:1000:18 --save  \
    --major num_flows ./classbench/{fw1,ipc2,acl5}.txt
```

[clslib]: https://gitlab.com/pschuprikov/classifiers-lib/tree/8389d307a523800b86a42846214fac1b4c8fc78b
[click]: http://click.pocoo.org/5/
[sqlalchemy]: https://www.sqlalchemy.org/
[python]: https://www.python.org/
