from collections import namedtuple

from cls.optimizations import sharing

from common import CommonData
from classifier_gen import ShareableLoader

SplitResult = namedtuple('SplitResult', ['shared', 'unshared'])


class Algorithm:
    def __call__(self, loader: ShareableLoader) -> SplitResult:
        raise NotImplementedError

    @staticmethod
    def instances():
        return {
            'parallel': ParallelAlgorithm(),
            'traditional': TraditionalAlgorithm(),
            'serial': SerialAlgorithm(),
            'layout': LayoutAlgorithm()
        }


class ParallelAlgorithm(Algorithm):
    async def __call__(self, loader: ShareableLoader):
        classifier = await loader.action_class_loader.load()
        shareable = await loader.load()

        shared, unshared = await CommonData.processor.schedule(
            self._run, classifier, shareable)
        return SplitResult(shared=len(shared), unshared=len(unshared))

    @staticmethod
    def _run(classifier, shareable):
        return sharing.split_shared_unshared(classifier, lambda x: x in shareable)


class TraditionalAlgorithm(Algorithm):
    async def __call__(self, loader: ShareableLoader):
        classifier = await loader.action_class_loader.load_optimized()
        return SplitResult(shared=0, unshared=len(classifier))


class LayoutAlgorithm(Algorithm):
    async def __call__(self, loader: ShareableLoader):
        classifier = await loader.action_class_loader.load_optimized()
        return SplitResult(shared=len(classifier), unshared=0)


class SerialAlgorithm(Algorithm):
    async def __call__(self, loader):
        classifier = await loader.action_class_loader.load_optimized()
        shareable = await loader.load()
        all_actions = set(e.action for e in classifier)
        return SplitResult(
            shared=len(classifier) + len(all_actions & shareable),
            unshared=len(all_actions - shareable))
