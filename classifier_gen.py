import os.path
from itertools import islice
from random import randrange, seed, sample, choices

import cls.optimizations.sharing as opt
from cls.parsers import parsing
from common import CommonData
from data import get_session
from utils import memoize, timeit


class RawClassLoader:
    def __init__(self, cb_input, num_rows):
        self._cb_input = cb_input
        self._num_rows = num_rows

    @property
    def name(self):
        return os.path.basename(self._cb_input.name)

    @property
    def num_rows(self):
        return self._num_rows

    @memoize
    async def load(self):
        return parsing.read_classifier(
            parsing.classbench_expanded,
            islice(self._cb_input, self._num_rows)
        )


class ActionClassLoader:
    def __init__(self, raw, action_classifier_point, idx):
        self._action_classifier_point = action_classifier_point
        self._idx = idx
        self._raw = raw

    @property
    def idx(self):
        return self._idx

    @property
    def action_classifier_point(self):
        return self._action_classifier_point

    @memoize
    async def load(self):
        return await CommonData.processor.schedule(
            create_action_classifier_ext, CommonData.dbname,
            await self._raw.load(), self.action_classifier_point, self.idx)

    @memoize
    async def load_optimized(self):
        return await CommonData.processor.schedule(
            opt.try_boolean_minimization, await self.load()
        )


class ShareableLoader:
    def __init__(self, action_class_loader: ActionClassLoader, classifier_point):
        self._action_class_loader = action_class_loader
        self._classifier_point = classifier_point

    @property
    def action_class_loader(self):
        return self._action_class_loader

    @property
    def classifier_point(self):
        return self._classifier_point

    @memoize
    async def load(self):
        return build_shareable(
            await self.action_class_loader.load(),
            self.classifier_point,
            self.action_class_loader.idx
        )


def create_action_classifier_ext(dbname, classifier, act_classifier_point, idx):
    session = get_session(dbname)
    try:
        act_classifier_point = session.merge(act_classifier_point)
        return create_action_classifier(classifier, act_classifier_point, idx)
    finally:
        session.close()


@timeit
def create_action_classifier(classifier, act_classifier_point, idx):
    classifier = classifier.subset(range(len(classifier)))
    actions = list(range(act_classifier_point.num_actions))
    action_weights = [1 / (i + 1) ** act_classifier_point.zipf_s
                      for i in range(act_classifier_point.num_actions)]

    seed(act_classifier_point.num_actions +
         act_classifier_point.min_class_size +
         act_classifier_point.max_class_size +
         int(act_classifier_point.zipf_s * 100) +
         idx)

    class_start_idx = 0
    while class_start_idx < len(classifier):
        class_end_idx = class_start_idx + randrange(
            act_classifier_point.min_class_size, act_classifier_point.max_class_size + 1
            )

        action, = choices(actions, weights=action_weights)
        for k in range(class_start_idx, min(class_end_idx, len(classifier))):
            classifier.vmr[k] = classifier[k]._replace(action=action)
        class_start_idx = class_end_idx
    return classifier


@timeit
def build_shareable(classifier, classifier_point, idx):
    seed(int(classifier_point.shareable_fraction * 100) +
         int(classifier_point.independency * 100) + idx)

    actions = list(range(classifier_point.action_classifier_point.num_actions))
    num_shareable = int(len(actions) * classifier_point.shareable_fraction)
    num_high_priority = int(num_shareable * classifier_point.independency)

    if num_high_priority > 0:
        weights = opt.weight_action_obstruction(classifier)
        actions = sorted(actions, key=lambda x: weights[x], reverse=True)

    return set(
        actions[:num_high_priority] +
        sample(actions[num_high_priority:], num_shareable - num_high_priority))
