from concurrent.futures import ProcessPoolExecutor
import asyncio
from asyncio import Semaphore
import logging

logging.basicConfig(
    filename='tcam_testing.log',
    level=logging.INFO,
    format='%(asctime)s %(levelname)s:%(message)s',
    datefmt='%d.%m.%Y %H:%M:%S')


class Processor:
    def __init__(self, num_processes):
        self._num_started = 0
        self._num_finished = 0
        self._executor = ProcessPoolExecutor(max_workers=num_processes)

    async def schedule(self, f, *args):
        return await asyncio.get_event_loop().run_in_executor(
            self._executor, f, *args)

    def shutdown(self):
        self._executor.shutdown()


class CommonData:
    dbname = 'data.db'
    session = None
    semaphore = None
    processor = None
    _finished_jobs = 0
    _total_jobs = 0

    @staticmethod
    def init(dbname, session, num_processes):
        CommonData.dbname = dbname
        CommonData.session = session
        CommonData.semaphore = Semaphore(2 * (num_processes or 1))
        CommonData.processor = Processor(num_processes or 1)

    @staticmethod
    def add(message):
        CommonData._total_jobs += 1
        print(f"+ {message} [{CommonData._finished_jobs}/{CommonData._total_jobs}]")

    @staticmethod
    def done(message):
        CommonData._finished_jobs += 1
        print(f"- {message} [{CommonData._finished_jobs}/{CommonData._total_jobs}]")
