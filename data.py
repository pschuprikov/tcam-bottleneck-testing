from collections import namedtuple
import enum

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, Float, String, Enum
from sqlalchemy import UniqueConstraint, ForeignKey
from sqlalchemy.orm import relationship
from utils import GetOrCreateMixin, LookupOrReserveMixin, SaveMixin, get_session_base


Base = declarative_base()


class ActionClassifierPoint(Base, GetOrCreateMixin):
    __tablename__ = 'action_classifier_points'

    id = Column(Integer, primary_key=True)
    classifier_name = Column(String)
    num_rows = Column(Integer)
    num_actions = Column(Integer)
    zipf_s = Column(Float)
    min_class_size = Column(Integer)
    max_class_size = Column(Integer)

    __tableargs__ = (UniqueConstraint(
        classifier_name, num_rows, num_actions, zipf_s, min_class_size, max_class_size
    ))

    def _lookup(self, session):
        return session.query(ActionClassifierPoint).filter(
            ActionClassifierPoint.classifier_name == self.classifier_name,
            ActionClassifierPoint.num_rows == self.num_rows,
            ActionClassifierPoint.num_actions == self.num_actions,
            ActionClassifierPoint.zipf_s == self.zipf_s,
            ActionClassifierPoint.min_class_size == self.min_class_size,
            ActionClassifierPoint.max_class_size == self.max_class_size,
        ).first()

    def __repr__(self):
        return (f'({self.classifier_name} with {self.num_actions} actions'
                f', s={self.zipf_s}'
                f'; class size {self.min_class_size}--{self.max_class_size})'
                )


class ClassifierPoint(Base, GetOrCreateMixin):
    __tablename__ = 'classifier_points'

    id = Column(Integer, primary_key=True)
    action_classifier_point_id = Column(
        Integer, ForeignKey(ActionClassifierPoint.id, ondelete='CASCADE'))
    action_classifier_point = relationship(ActionClassifierPoint)
    shareable_fraction = Column(Float)
    independency = Column(Float)

    __tableargs__ = (UniqueConstraint(
        action_classifier_point_id, shareable_fraction, independency
    ))

    def _lookup(self, session):
        return session.query(ClassifierPoint).filter(
            ClassifierPoint.action_classifier_point == self.action_classifier_point,
            ClassifierPoint.shareable_fraction == self.shareable_fraction,
            ClassifierPoint.independency == self.independency
        ).first()

    def __repr__(self):
        return (
            f'{self.action_classifier_point}'
            f'[shareable={self.shareable_fraction}, ind={self.independency}]')


class AlgorithmResult(Base, GetOrCreateMixin, LookupOrReserveMixin, SaveMixin):
    __tablename__ = 'algorithm_results'

    id = Column(Integer, primary_key=True)
    algo = Column(String)
    classifier_point_id = Column(Integer, ForeignKey(ClassifierPoint.id, ondelete='CASCADE'))
    classifier_point = relationship(ClassifierPoint)
    instance_idx = Column(Integer)
    shared = Column(Integer)
    unshared = Column(Integer)
    cookie = Column(Integer)

    __tableargs__ = (UniqueConstraint(
        algo, classifier_point_id, instance_idx
    ))

    def is_ready(self):
        return self.shared is not None

    def _lookup(self, session):
        return session.query(AlgorithmResult).filter(
            AlgorithmResult.algo == self.algo,
            AlgorithmResult.classifier_point == self.classifier_point,
            AlgorithmResult.instance_idx == self.instance_idx
        ).first()

    def __repr__(self):
        return '[for {} {} produces {}(u) and {}(s)]'.format(
            self.classifier_point, self.algo, self.unshared, self.shared)


class Result(Base, GetOrCreateMixin, SaveMixin):
    __tablename__ = 'results'

    id = Column(Integer, primary_key=True)
    algorithm_result_id = Column(
        Integer, ForeignKey(AlgorithmResult.id, ondelete='CASCADE'))
    num_flows = Column(Integer)
    total = Column(Integer)
    algorithm_result = relationship(AlgorithmResult)

    __tableargs__ = (
        UniqueConstraint('algorithm_result_id', 'num_flows')
    )

    def _lookup(self, session):
        return session.query(Result).filter(
            Result.algorithm_result == self.algorithm_result,
            Result.num_flows == self.num_flows
        ).first()


run_result = Table(
    'run_result', Base.metadata,
    Column('run_id', ForeignKey('runs.id', ondelete='CASCADE'), primary_key=True),
    Column('result_id', ForeignKey(Result.id, ondelete='CASCADE'), primary_key=True)
)


Parameter = namedtuple('Variable', ['f'])


class VariableEnum(enum.Enum):
    NUM_ACTIONS = Parameter(
        lambda r:
        r.algorithm_result.classifier_point.action_classifier_point.num_actions
    )
    SHAREABLE_FRACTION = Parameter(
        lambda r: r.algorithm_result.classifier_point.shareable_fraction
    )
    NUM_FLOWS = Parameter(lambda r: r.num_flows)
    INDEPENDENCY = Parameter(
        lambda r: r.algorithm_result.classifier_point.independency
    )
    MAX_CLASS_SIZE = Parameter(
        lambda r:
        r.algorithm_result.classifier_point.action_classifier_point.max_class_size
    )
    ZIPF_S = Parameter(
        lambda r:
        r.algorithm_result.classifier_point.action_classifier_point.zipf_s
    )


class FixedEnum(enum.Enum):
    CLASSIFIER = Parameter(
        lambda r:
        r.algorithm_result.classifier_point.action_classifier_point.classifier_name
    )
    MIN_CLASS_SIZE = Parameter(
        lambda r:
        r.algorithm_result.classifier_point.action_classifier_point.min_class_size
    )


class Run(Base, SaveMixin):
    __tablename__ = 'runs'

    id = Column(Integer, primary_key=True)
    num_runs = Column(Integer)
    variable = Column(Enum(*(x.name for x in VariableEnum)))
    range = Column(String)
    algos = Column(String)
    results = relationship(Result, secondary=run_result)


def get_session(dbname):
    return get_session_base(dbname, Base)
