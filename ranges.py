from collections import namedtuple

from click import ParamType


class PointRange(namedtuple('PointRange', ['value'])):
    __slots__ = ()

    def __iter__(self):
        return iter([self.value])

    def __repr__(self):
        return f'{self.value}'

    @classmethod
    def from_str(cls, value):
        return cls(float(value))


class Range(namedtuple('Range', ['start', 'end', 'num'])):
    __slots__ = ()

    def __iter__(self):
        return (
            self.start + i * (self.end - self.start) / self.num
            for i in range(self.num + 1)
        )

    def __repr__(self):
        return f'{self.start}:{self.end}:{self.num}'

    @classmethod
    def from_str(cls, value):
        start, end, num = value.split(":")
        return cls(float(start), float(end), int(num))


class ExpRange(namedtuple('ExpRange', ['start', 'end', 'num'])):
    __slots__ = ()

    def __iter__(self):
        print(self.end / self.start)
        return (
            self.start * ((self.end / self.start) ** (1 / self.num)) ** i
            for i in range(self.num + 1)
        )

    def __repr__(self):
        return f'{self.start}:{self.end}:{self.num}:exp'

    @classmethod
    def from_str(cls, value):
        start, end, num, exp = value.split(":")
        if exp != 'exp':
            raise ValueError
        return cls(float(start), float(end), int(num))


class RangeParam(ParamType):
    name = 'range'

    def convert(self, value, param, ctx):
        try:
            return PointRange.from_str(value)
        except ValueError:
            pass
        try:
            return ExpRange.from_str(value)
        except ValueError:
            pass
        return Range.from_str(value)
