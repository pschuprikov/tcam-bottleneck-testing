import asyncio
import csv
import os.path
from collections import OrderedDict
from itertools import product, groupby, chain

import click

import data
from algorithms import Algorithm
from classifier_gen import ShareableLoader, RawClassLoader, ActionClassLoader
from common import CommonData, logging
from data import FixedEnum, VariableEnum
from ranges import RangeParam
from utils import keep_trying


async def _run_for_point(shl: ShareableLoader, algo):
    """ Run a specified algorithm on a specified classifier point

    Args:
        classifier_point (Class): classifier generator
        algo: name of the algorithm to run

    Returns (AlgorithmResult):
        The result of algorithm's run
    """

    # noinspection PyArgumentList
    result = await keep_trying(data.AlgorithmResult(
        algo=algo, classifier_point=shl.classifier_point, cookie=os.getpid(),
        instance_idx=shl.action_class_loader.idx
    ).lookup_or_reserve, num_iterations=None, no_none=True)(session=CommonData.session)

    if result.is_ready():
        logging.info(f'Result of running {shl.classifier_point} with {algo}'
                     f' (#{shl.action_class_loader.idx}) was found in DB')
        return result

    logging.info(f'running {shl.classifier_point} with {algo}'
                 f' (#{shl.action_class_loader.idx})')

    split = await Algorithm.instances()[algo](shl)

    result.shared = split.shared
    result.unshared = split.unshared
    result.cookie = None

    logging.info(f'finished {shl.classifier_point} with {algo}'
                 f' (#{shl.action_class_loader.idx})')

    await keep_trying(result.save)(session=CommonData.session)

    return result


async def _run_all_algos_for_point(shl: ShareableLoader, algos):
    try:
        CommonData.add(f'{shl.classifier_point} (#{shl.action_class_loader.idx})')
        await CommonData.semaphore.acquire()
        result = await asyncio.gather(*(_run_for_point(shl, algo) for algo in algos))
        CommonData.done(f'{shl.classifier_point} (#{shl.action_class_loader.idx})')
        return result
    finally:
        CommonData.semaphore.release()


async def _run_all_algos(shls, algos):
    loop = asyncio.get_event_loop()
    per_shl_results = []
    async for shl in shls:
        per_shl_results.append(loop.create_task(_run_all_algos_for_point(shl, algos)))
    return chain(*(await asyncio.gather(*per_shl_results)))


def _params_to_str(run, enum):
    first_result = run.results[0]

    variables = []
    for v in enum:
        if enum is VariableEnum and v == enum[run.variable]:
            value = run.range.replace(':', '_')
        else:
            value = str(v.value.f(first_result))
        variables.append(f'{v.name.lower()}-{value}')

    return '.'.join(variables)


def do_save_run(run_id, output=None, output_dir='output'):
    run = CommonData.session.query(data.Run).filter(data.Run.id == run_id).first()

    ys = OrderedDict(shared=lambda r: r.algorithm_result.shared,
                     unshared=lambda r: r.algorithm_result.unshared,
                     total=lambda r: r.total)

    if output is None:
        output = (_params_to_str(run, FixedEnum) + '.' +
                  _params_to_str(run, VariableEnum)
                  + f'.num_runs-{run.num_runs}.tsv')

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    with open(os.path.join(output_dir, output), 'w') as tsv_file:
        print(f'Saving run to {tsv_file.name}...')
        fieldnames = ['variable'] + [
            f'{a}_{v}' for a in run.algos.split(',') for v in ys.keys()
        ]
        writer = csv.DictWriter(tsv_file, fieldnames=fieldnames,
                                dialect=csv.excel_tab)
        writer.writeheader()
        for var, val in _collect_run(run, ys):
            writer.writerow(val)


async def gen_acls(raws, num_actions, minc, maxcs, zipf_s, num_runs):
    for raw_cls, na, maxc, s in product(raws, num_actions, maxcs, zipf_s):
        # noinspection PyArgumentList
        acp = await keep_trying(data.ActionClassifierPoint(
            classifier_name=raw_cls.name, num_rows=raw_cls.num_rows,
            min_class_size=int(minc), max_class_size=int(maxc),
            num_actions=int(na), zipf_s=s
        ).get_or_create)(session=CommonData.session)
        for idx in range(num_runs):
            yield ActionClassLoader(raw_cls, acp, idx)


async def gen_shls(acls, shareable_fraction, independency):
    async for acl in acls:
        for sf, ind in product(shareable_fraction, independency):
            # noinspection PyArgumentList
            cp = await keep_trying(data.ClassifierPoint(
                action_classifier_point=acl.action_classifier_point,
                shareable_fraction=sf, independency=ind
            ).get_or_create)(session=CommonData.session)
            yield ShareableLoader(acl, cp)


async def do_run(major, num_actions, zipf_s, min_class_size, max_class_size,
                 independency, shareable_fraction, num_flows, algo, raws, num_runs):
    algorithm_results = await _run_all_algos(
        gen_shls(gen_acls(raws, num_actions, min_class_size, max_class_size, zipf_s, num_runs),
                 shareable_fraction, independency), algo)

    # noinspection PyArgumentList
    results = await asyncio.gather(*(
        keep_trying(data.Result(
            algorithm_result=result, num_flows=nf,
            total=nf * result.unshared + result.shared
        ).get_or_create)(session=CommonData.session)
        for result in algorithm_results for nf in num_flows
    ))

    variables = {
        data.VariableEnum.NUM_FLOWS: num_flows,
        data.VariableEnum.NUM_ACTIONS: num_actions,
        data.VariableEnum.SHAREABLE_FRACTION: shareable_fraction,
        data.VariableEnum.INDEPENDENCY: independency,
        data.VariableEnum.MAX_CLASS_SIZE: max_class_size,
        data.VariableEnum.ZIPF_S: zipf_s
    }
    assert (set(variables) == set(
        data.VariableEnum)), "You forgot to add variable!!!"

    if major is not None:
        var = data.VariableEnum[major.upper()]
        (r, v) = variables[var], var
        if len(r) == 1:
            print('Major variable does not vary')
            return
    else:
        try:
            (r, v), = ((r, v) for v, r in variables.items() if len(r) > 1)
        except ValueError:
            print('To count as a run, exactly one variable must vary')
            return

    print('Adding Run(s)')

    fixed_vars = list(data.FixedEnum) + [vv for vv in data.VariableEnum if vv is not v]
    fixed_key = lambda res: tuple(x.value.f(res) for x in fixed_vars)

    runs = []
    for name, results in groupby(sorted(results, key=fixed_key), key=fixed_key):
        # noinspection PyArgumentList
        runs.append(data.Run(
            variable=v.name, range=repr(r), algos=",".join(sorted(algo)),
            num_runs=num_runs, results=list(results)))
        await keep_trying(runs[-1].save)(session=CommonData.session)

    return runs


@click.group()
@click.option('--data-file', type=str, default=CommonData.dbname,
              help='SQL3 file where to put the results')
@click.option('--num-processes', type=int,
              help='Number of processes to use')
def common(data_file, num_processes):
    CommonData.init(session=data.get_session(data_file),
                    dbname=data_file, num_processes=num_processes)


@common.command()
def cleanup():
    _cleanup()


@common.command()
def list_runs():
    for run in CommonData.session.query(data.Run).all():
        first_result = run.results[0]
        print(
            f'[{run.id}] {run.variable}({run.range}) for {run.algos}' +
            f' with num_flows = {first_result.num_flows} and ' +
            f' classifier = {first_result.algorithm_result.classifier_point}'
            f' #={run.num_runs}'
        )


@common.command()
@click.argument('run_id', nargs=-1, type=int)
def delete_run(run_id):
    CommonData.session.query(data.Run).filter(data.Run.id.in_(list(run_id))).delete(
        synchronize_session=False
    )
    CommonData.session.commit()


@common.command()
@click.option('--output', type=str,
              help='Output file to use')
@click.option('--dir', default='output', type=str,
              help='Directory where to save outputs')
@click.argument('run_id', type=int)
def save_run(output, dir, run_id):
    do_save_run(run_id, output, dir)


@common.command()
@click.option('--output', type=str,
              help='Output file to use')
@click.option('--dir', default='output', type=str,
              help='Directory where to save outputs')
def save_all_runs(output, dir):
    for run in CommonData.session.query(data.Run).all():
        do_save_run(run.id, output, dir)


class Average:
    def __init__(self):
        self._count = 0
        self._value = 0

    def __str__(self):
        return str(self._value)

    def update(self, value):
        self._value += 1 / (self._count + 1) * (value - self._value)
        self._count += 1


def _collect_run(run, ys):
    result = {}
    x = VariableEnum[run.variable].value.f
    for r in run.results:
        if x(r) not in result:
            result[x(r)] = {'variable': x(r)}
        algo = r.algorithm_result.algo
        for yname, y in ys.items():
            column = f'{algo}_{yname}'
            if column not in result[x(r)]:
                result[x(r)][column] = Average()
            result[x(r)][f'{algo}_{yname}'].update(y(r))

    return sorted(result.items(), key=lambda a: a[0])


def _cleanup(cookie=None):
    CommonData.session.rollback()
    query = CommonData.session.query(data.AlgorithmResult).filter(
        data.AlgorithmResult.shared.is_(None))
    if cookie is not None:
        query = query.filter(data.AlgorithmResult.cookie == cookie)
    query.delete(synchronize_session=False)
    CommonData.session.commit()


def _flatten(tpl):
    if len(tpl) == 1:
        return tpl[0]
    else:
        return tuple(chain(*tpl))


@common.command()
@click.option('--num-actions', type=RangeParam(), required=True, multiple=True,
              help='Number of supported actions')
@click.option('--zipf-s', type=RangeParam(), required=True, default='0', multiple=True,
              help='s-Parameter of the ZipF distribution')
@click.option('--min-class-size', type=int, default=5, required=True)
@click.option('--max-class-size', type=RangeParam(), required=True, multiple=True,
              help='Maximal size of a class')
@click.option('--independency', type=RangeParam(), required=True, multiple=True,
              default='0', help='Level of independency')
@click.option('--shareable-fraction', type=RangeParam(), required=True, multiple=True,
              help='Shareable fraction of actions')
@click.option('--num-flows', type=RangeParam(), required=True, multiple=True,
              help='Number of flows')
@click.option('--algo', type=click.Choice(Algorithm.instances().keys()),
              multiple=True, help='Algorithm')
@click.option('--num-rows', type=int,
              help='Limit on the number of lines read from classbench file')
@click.option('--save', is_flag=True,
              help='Whether to save the run to tsv')
@click.option('--major',
              type=click.Choice(tuple(x.name.lower() for x in VariableEnum)))
@click.option('--num-runs', type=int, default=1,
              help='Number of runs for each point')
@click.argument('classbench-input', nargs=-1, type=click.File('r'),
                required=True)
def run(major, num_actions, zipf_s, min_class_size, max_class_size,
        independency, shareable_fraction, num_flows, algo, num_rows, save,
        num_runs, classbench_input):
    raws = (RawClassLoader(cbi, num_rows) for cbi in classbench_input)

    if not algo:
        algo = list(Algorithm.instances().keys())

    loop = asyncio.get_event_loop()
    try:
        runs = loop.run_until_complete(do_run(
            major, _flatten(num_actions), _flatten(zipf_s), min_class_size,
            _flatten(max_class_size), _flatten(independency),
            _flatten(shareable_fraction), _flatten(num_flows), algo, raws, num_runs))
    finally:
        CommonData.processor.shutdown()
        _cleanup(os.getpid())

    if save and runs is not None:
        for run in runs:
            do_save_run(run.id)


if __name__ == '__main__':
    common()
