{ pkgs ? import <nixpkgs> {} }:

let 
  python = pkgs.python3.withPackages (ps: 
      with ps; [click numpy pylint ipython sqlalchemy ]);

in pkgs.stdenv.mkDerivation {
  name = "test";
  src = ./.;

  inherit python;
  buildInputs = [ python ];

  PYTHONPATH = "../classifiers-lib/:../classifiers-lib/p4t_native/build";
}

