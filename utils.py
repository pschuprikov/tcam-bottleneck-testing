import asyncio
import logging
import time
from functools import wraps

from sqlalchemy import event, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.exc import IntegrityError, OperationalError
from sqlalchemy.orm import sessionmaker


@event.listens_for(Engine, 'connect')
def _set_sqlite_pragma(dbapi_connection, _):
    cursor = dbapi_connection.cursor()
    cursor.execute('pragma foreign_keys = on')
    cursor.close()


def get_session_base(dbname, base_cls):
    engine = create_engine('sqlite:///{}'.format(dbname))
    base_cls.metadata.create_all(engine, checkfirst=True)
    DBSession = sessionmaker(bind=engine)
    return DBSession()


class Lookupable:
    def _lookup(self, session):
        raise NotImplementedError


class SaveMixin:
    def save(self, session):
        session.add(self)
        session.commit()


# noinspection PyAbstractClass
class GetOrCreateMixin(Lookupable):
    def get_or_create(self, session):
        result = self._lookup(session)

        if result is not None:
            return result

        session.add(self)
        return self


# noinspection PyAbstractClass
class LookupOrReserveMixin(Lookupable):
    def is_ready(self):
        raise NotImplementedError

    def lookup_or_reserve(self, session):
        result = self._lookup(session)
        if result is not None:
            if result.is_ready():
                return result
            else:
                return None
        session.add(self)
        return self


class ResultIsNone(Exception):
    pass


def keep_trying(func, num_iterations=10, no_none=False):
    async def wrapped(*args, session, **kwargs):
        current_iteration = 0
        while True:
            try:
                result = func(*args, session=session, **kwargs)
                session.commit()
                if no_none and result is None:
                    raise ResultIsNone()
                return result
            except (IntegrityError, OperationalError, ResultIsNone):
                session.rollback()
                if current_iteration == num_iterations:
                    raise
                if current_iteration > 10 and (current_iteration % 10) == 0:
                    logging.warning(
                        f'Waiting {current_iteration} iterations '
                        f'for {func.__name__} to complete'
                    )
                current_iteration += 1
                await asyncio.sleep(1)
    return wrapped


def memoize(method):
    @wraps(method)
    async def memoized(self, *args, **kwargs):
        cache_name = f'__{method.__name__}_result'
        lock_name = f'__{method.__name__}_lock'
        if not hasattr(self, lock_name):
            setattr(self, lock_name, asyncio.Lock())
        async with getattr(self, lock_name):
            if not hasattr(self, cache_name):
                setattr(self, cache_name, await method(self, *args, **kwargs))
        return getattr(self, cache_name)
    return memoized


def timeit(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        start_time = time.time()
        result = f(*args, **kwargs)
        end_time = time.time()
        logging.info(f'execution of {f.__name__} took {(end_time - start_time) * 1000} ms')
        return result
    return wrapped
